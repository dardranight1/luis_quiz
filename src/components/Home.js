import React, { Component } from 'react'

export default class Home extends Component { 

    render() {
        return (
            <div className="col-12 text-center">
                <h2 className="title">Welcome to The Trivia Challenge!</h2>
                <div className="mainText">You will be pressented
                    with 10 True or False
                    questions.
                </div>
                <div>Can you score 100%?</div>
            </div>
        )
    }
}
