import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './cutom.css'

//Components
import Quiz from './Quiz';
import Home from './Home';

export default class App extends Component {

  render() {
    return (
      <div className="row text-center centered ">
        <Router>
          <Route exact path="/" render={() =>{
            return <div className="col-12 text-center">
              {<Home></Home>}
              <Link to="/Quiz" className="link">Begin</Link>
            </div>
          }}>
          </Route>
          <Route exact path="/Quiz" render={() =>{
            return <div className="col-12 text-center centered">
              {<Quiz></Quiz>}
              <Link to="/" className="link">Home</Link>
            </div>
          }}>
          </Route>
        </Router>
      </div>
    )
  }
}
