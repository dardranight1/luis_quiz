import React, { Component } from 'react'

import Questions from './Questions'

export default class Quiz extends Component {
    
    render() {
        return (
            <div className="col-12 text-center centered">
              <Questions></Questions>
            </div>
        )
    }
}
