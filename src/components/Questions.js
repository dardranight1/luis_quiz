import React, { Component } from 'react'

export default class Questions extends Component {
    
    state = {
        questions: [],
        index: 0,
        results: [],
        goodResuls: 0
    }

    async componentDidMount(){
        const res = await fetch('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean')
        const data = await res.json();
        this.setState({questions: data.results})
    }

    check(e, respuesta) {
        e.preventDefault();
        if (this.state.questions[this.state.index].correct_answer === respuesta) {
            this.setState({index: this.state.index + 1})
            var newStateArray = this.state.results.slice();
            newStateArray.push(true);
            this.setState({results: newStateArray});
            this.setState({goodResuls: this.state.goodResuls + 1});
            console.log("Respondio bien");
        }
        else{
            this.setState({index: this.state.index + 1})
            var newStateArray = this.state.results.slice();
            newStateArray.push(false);
            this.setState({results: newStateArray});
            console.log("No respondio bien");
        }
    }

    render() {
        if (this.state.index !== 10) {
            return (
                <div className="card text-white bg-dark mb-3 quest text-center centered">
                    <div className="card-header text-center">{this.state.questions[this.state.index]?.category || ""}</div>
                    <div className="card-body text-center">{this.state.questions[this.state.index]?.question.replace(/&quot;/g, '"') || ""}</div>
                    <div className="btn-group">
                        <button className="btn btn-light" onClick={(e) => this.check(e,"True")}>Yes</button>
                        <button className="btn btn-light" onClick={(e) => this.check(e,"False")}>No</button>
                    </div>
                    <p className="text-center">{this.state.index + 1} of 10</p>
                </div>
            )   
        }
        else{
            return (
                <div className="col-12 text-center">
                    <h2 className="col-12 text-center">You Scored</h2>
                    <h3 className="col-12 text-center">{this.state.goodResuls}/10</h3>
                    {this.state.results.map((result, index) => {
                        return <div className="row Results">
                            <div className="col-2">{result? "+" : "-"}</div>
                            <div className="col-10">{this.state.questions[index].question.replace(/&quot;/g, '"')}</div>
                        </div>
                    })}
                </div>
            )
        }
    }
}